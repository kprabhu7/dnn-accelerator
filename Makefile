build/conv.v1/rtl.v: build/SystolicArray*.v1/rtl.v build/SIMDUnit*.v1/rtl.v build/WeightFetcher*.v1/rtl.v build/BiasCombiner*.v1/rtl.v build/WeightOrganizer*.v1/rtl.v build/InputBuffer*.v1/rtl.v build/IdentityFetcher*.v1/rtl.v
	catapult -shell -file scripts/conv.tcl
	sed '/module CGHpart/,/endmodule/d;/module TSDN/,/endmodule/d;/^`include/d' build/conv.v1/concat_rtl.v > build/conv.v1/concat_rtl_clean.v

build/SystolicArray*.v1/rtl.v: src/SystolicArray.h
	catapult -shell -file scripts/SystolicArray.tcl

SystolicArray: build/SystolicArray*.v1/rtl.v

build/InputSkewer*.v1/rtl.v: src/InputSkewer.h
		catapult -shell -file scripts/InputSkewer.tcl

InputSkewer: build/InputSkewer*.v1/rtl.v

build/PEArray*.v1/rtl.v: src/PEArray.h
	catapult -shell -file scripts/PEArray.tcl

PEArray: build/PEArray*.v1/rtl.v

build/ProcessingElement*.v1/rtl.v: src/ProcessingElement.h
	catapult -shell -file scripts/ProcessingElement.tcl

ProcessingElement: build/ProcessingElement*.v1/rtl.v

build/WeightFetcher*.v1/rtl.v: src/WeightFetcher.h
	catapult -shell -file scripts/WeightFetcher.tcl

WeightFetcher: build/WeightFetcher*.v1/rtl.v

build/BiasCombiner*.v1/rtl.v: src/WeightFetcher.h
	catapult -shell -file scripts/BiasCombiner.tcl

BiasCombiner: build/BiasCombiner*.v1/rtl.v

build/WeightOrganizer*.v1/rtl.v: src/WeightFetcher.h
	catapult -shell -file scripts/WeightOrganizer.tcl

WeightOrganizer: build/WeightOrganizer*.v1/rtl.v

build/InputBuffer*.v1/rtl.v: src/InputBuffer.h
	catapult -shell -file scripts/InputBuffer.tcl

InputBuffer: build/InputBuffer*.v1/rtl.v

build/SIMDUnit*.v1/rtl.v: src/SIMDUnit.h
	catapult -shell -file scripts/SIMDUnit.tcl

SIMDUnit: build/SIMDUnit*.v1/rtl.v

build/IdentityFetcher*.v1/rtl.v: src/IdentityFetcher.h
	catapult -shell -file scripts/IdentityFetcher.tcl

IdentityFetcher: build/IdentityFetcher*.v1/rtl.v

resnet18_test/conv.v1:
	catapult -shell -file scripts/run_resnet18_test.tcl

resnet18_test: resnet18_test/conv.v1
	cd resnet18_test/conv.v1 && make -f scverify/Verify_orig_cxx_osci.mk clean
	cd resnet18_test/conv.v1 && make -f scverify/Verify_orig_cxx_osci.mk sim | tee resnet18_output.txt

gui:
	catapult build.ccs

sim: build/conv.v1/rtl.v
	cd build/conv.v1 && make -f scverify/Verify_concat_sim_rtl_v_ncsim.mk SIMTOOL=ncsim simgui

.PHONY: gui clean c_test clean_c_test SystolicArray InputSkewer PERow ProcessingElement WeightFetcher InputBuffer SIMDUnit
clean:
	rm build.ccs
	rm -rf build

clean_c_test:
	rm c_test.ccs
	rm -rf c_test

clean_c_test_debug:
	rm -rf pe_debug/
	rm c_test_debug.ccs
	rm -rf c_test_debug

clean_resnet_test:
	rm resnet_test.ccs
	rm -rf resnet_test

clean_resnet_test_debug:
	rm -rf pe_debug/
	rm resnet_test_debug.ccs
	rm -rf resnet_test_debug
