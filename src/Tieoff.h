// Copyright 2022 The Google Research Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef THIRD_PARTY_DNN_ACCELERATOR_HLS_SRC_TIEOFF_H_
#define THIRD_PARTY_DNN_ACCELERATOR_HLS_SRC_TIEOFF_H_

#include <mc_connections.h>
#include <systemc.h>

template <class T>
SC_MODULE(Tieoff) {
 public:
  Connections::In<T> CCS_INIT_S1(in);

  SC_CTOR(Tieoff) {
    SC_METHOD(drive_rdy);
    sensitive << in.rdy;

#ifdef CONNECTIONS_SIM_ONLY
    in.disable_spawn();
#endif
  }

  void drive_rdy() { in.rdy = 1; }
};

#endif  // THIRD_PARTY_DNN_ACCELERATOR_HLS_SRC_TIEOFF_H_
