// Copyright 2022 The Google Research Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef THIRD_PARTY_DNN_ACCELERATOR_HLS_SRC_ARCHITECTUREPARAMS_H_
#define THIRD_PARTY_DNN_ACCELERATOR_HLS_SRC_ARCHITECTUREPARAMS_H_

#define INPUT_DATATYPE ac::bfloat16
#define WEIGHT_DATATYPE ac::bfloat16
#define OUTPUT_DATATYPE ac::bfloat16

// #define INPUT_DATATYPE ac_int<32, true>
// #define WEIGHT_DATATYPE ac_int<32, true>
// #define OUTPUT_DATATYPE ac_int<32, true>

#define DIMENSION 4

#define INPUT_BUFFER_SIZE 1024
#define WEIGHT_BUFFER_SIZE 1024
#define ACCUM_BUFFER_SIZE 1024
#define NUM_CORES 1

#endif  // THIRD_PARTY_DNN_ACCELERATOR_HLS_SRC_ARCHITECTUREPARAMS_H_
