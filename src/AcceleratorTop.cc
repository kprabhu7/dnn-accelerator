// Copyright 2022 The Google Research Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "src/AcceleratorTop.h"

void AcceleratorTop::run() {
  paramsIn.Reset();
  vectorParamsIn.Reset();
  for(int i = 0; i < NUM_CORES; i++){
    paramsChannel[i].ResetWrite();
    vectorParamsChannel[i].ResetWrite();
  }
  wait();

  while (true) {
    Params params = paramsIn.Pop();
#pragma hls_unroll yes
    for (int i = 0; i < NUM_CORES; i++) {
      paramsChannel[i].Push(params);
    }

    VectorParams vectorParams = vectorParamsIn.Pop();
#pragma hls_unroll yes
    for (int i = 0; i < NUM_CORES; i++) {
      vectorParamsChannel[i].Push(vectorParams);
    }
  }
}
