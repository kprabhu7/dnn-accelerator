// Copyright 2022 The Google Research Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef THIRD_PARTY_DNN_ACCELERATOR_HLS_TEST_COMMON_OPERATIONMAPPER_H_
#define THIRD_PARTY_DNN_ACCELERATOR_HLS_TEST_COMMON_OPERATIONMAPPER_H_

#include "src/Params.h"
#include "test/common/operation.proto.h"

void MapConvolution(
    const third_party::dnn_accelerator_hls::test::common::ConvolutionOperation
        &operation,
    Params &params);
void MapMatMul(const third_party::dnn_accelerator_hls::test::common::
                   MatrixMultiplicationOperation &operation,
               Params &params);
void MapReshapes(
    const third_party::dnn_accelerator_hls::test::common::TensorOperation
        &operation,
    Params &params);
void MapTensorOperation(
    const third_party::dnn_accelerator_hls::test::common::TensorOperation
        &operation,
    Params &params, VectorParams &vectorParams);
void MapSoftmax(
    const third_party::dnn_accelerator_hls::test::common::Softmax &softmaxOp,
    VectorParams &vectorParams);
void MapLayerNorm(
    const third_party::dnn_accelerator_hls::test::common::LayerNorm
        &layerNormOp,
    VectorParams &vectorParams);
void MapOperationToAccelerator(
    const third_party::dnn_accelerator_hls::test::common::Operation &operation,
    Params &params, VectorParams &vectorParams);

#endif  // THIRD_PARTY_DNN_ACCELERATOR_HLS_TEST_COMMON_OPERATIONMAPPER_H_
