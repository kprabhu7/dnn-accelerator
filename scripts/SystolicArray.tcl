set blockname [file rootname [file tail [info script] ]]

source scripts/common.tcl

directive set -DESIGN_HIERARCHY { 
    {SystolicArray<IDTYPE, ODTYPE, 9, 16, 16>}
}

go compile

source scripts/set_libraries.tcl

go libraries
directive set -CLOCKS $clocks

directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16> -REGISTER_THRESHOLD 4096

go assembly

directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/run -DESIGN_GOAL Latency
directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/run -CLOCK_OVERHEAD 0.000000
directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/input:rsc -MAP_TO_MODULE ccs_ioport.ccs_in_pipe
directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/input:rsc -PACKING_MODE sidebyside

directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/input -WORD_WIDTH 1152

directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/weight:rsc -MAP_TO_MODULE ccs_ioport.ccs_in_pipe
directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/weight:rsc -PACKING_MODE sidebyside

directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/run/accumulation_buffer.value -WORD_WIDTH 288

directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/run/xy_i:in_col.value:rsc -MAP_TO_MODULE {[Register]}
directive set /SystolicArray<IDTYPE,ODTYPE,9,16,16>/run/xy_i:input_buf.value:rsc -MAP_TO_MODULE {[Register]}

go architect

ignore_memory_precedences -from WRITE_ACC_BUFFER* -to READ_ACC_BUFFER*

go allocate
go extract
