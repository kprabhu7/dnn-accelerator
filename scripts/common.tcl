set project_folder ./build/Catapult_$blockname

if { [file exists $project_folder] } {
    file delete -force -- $project_folder
}

project new -dir $project_folder
project save

options set Project/SolutionName $blockname
logfile move ./build/${blockname}.log

options set Input/TargetPlatform x86_64
options set /Input/CppStandard c++11
solution options set /Input/CppStandard c++11
options set Input/SearchPath $::env(BOOST_HOME)
options set Input/SearchPath /workspace/ -append
options set Output/OutputVHDL false
options set Architectural/DefaultRegisterThreshold 4096
options set Output/SubBlockNamePrefix $blockname

options set Flows/Enable-SCVerify yes
flow package require /SCVerify
flow package option set /SCVerify/USE_CCS_BLOCK true
flow package option set /SCVerify/USE_NCSIM false
flow package option set /SCVerify/USE_VCS true
flow package option set /SCVerify/USE_MSIM false
options set Flows/VCS/SYSC_VERSION 2.3.2

# Delete solution if already exists
catch {
    set existing_solution [project get /SOLUTION/$blockname* -match glob -return leaf]
    solution remove -solution $existing_solution -delete
}

set clocks {clk {-CLOCK_PERIOD 20 -CLOCK_EDGE rising -CLOCK_HIGH_TIME 10 -CLOCK_OFFSET 0.000000 -CLOCK_UNCERTAINTY 0.0 -RESET_KIND async -RESET_SYNC_NAME rst -RESET_SYNC_ACTIVE high -RESET_ASYNC_NAME arst_n -RESET_ASYNC_ACTIVE low -ENABLE_NAME {} -ENABLE_ACTIVE high}}

go new

solution file add ./third_party/dnn_accelerator_hls/src/AcceleratorTop.cc
solution file add ./third_party/dnn_accelerator_hls/test/common/AcceleratorHarness.cc -exclude true
solution file add ./third_party/dnn_accelerator_hls/test/common/GoldModel.cc -exclude true
solution file add ./third_party/dnn_accelerator_hls/test/common/OperationMapper.cc -exclude true
solution file add ./third_party/dnn_accelerator_hls/test/common/Utils.cc -exclude true
solution file add ./third_party/dnn_accelerator_hls/test/common/TestRunner.cc -exclude true
solution file add ./third_party/dnn_accelerator_hls/test/common/operation.pb.cc -exclude true

go analyze

solution design set $full_blockname -top

if {$blockname == "VectorUnit"} {
    solution design set "FetchUnit<$DIMENSION>" -mapped 
    solution design set "ScaleUnit<$OUTPUT_DATATYPE, $DIMENSION, $DIMENSION>" -mapped
    solution design set "LayerNormUnit<$OUTPUT_DATATYPE, $DIMENSION>" -mapped
    solution design set "OutputAddressGenerator<$OUTPUT_DATATYPE, $DIMENSION, $DIMENSION>" -mapped
    solution design set "SoftmaxUnit<$OUTPUT_DATATYPE, $DIMENSION>" -mapped
}

if {$blockname == "TensorCore"} {
    solution design set "InputController<$INPUT_DATATYPE, $DIMENSION>" -mapped
    solution design set "WeightController<$INPUT_DATATYPE, $DIMENSION, $DIMENSION>" -mapped
    solution design set "MatrixProcessor<$INPUT_DATATYPE, $WEIGHT_DATATYPE, $OUTPUT_DATATYPE, $DIMENSION, $DIMENSION, $ACCUM_BUFFER_SIZE>" -mapped
    solution design set "VectorUnit<$OUTPUT_DATATYPE, $DIMENSION, $DIMENSION>" -mapped
}

if {$blockname == "AcceleratorTop"} {
    solution design set "TensorCore<$INPUT_DATATYPE, $WEIGHT_DATATYPE, $OUTPUT_DATATYPE, $DIMENSION, $DIMENSION, $INPUT_BUFFER_SIZE, $WEIGHT_BUFFER_SIZE, $ACCUM_BUFFER_SIZE>" -mapped
}

go compile

solution options set ComponentLibs/SearchPath {./build/} -append

solution library add nangate-45nm_beh -- -rtlsyntool DesignCompiler -vendor Nangate -technology 045nm
solution library add ccs_sample_mem -- -rtlsyntool DesignCompiler -vendor Nangate -technology 045nm
