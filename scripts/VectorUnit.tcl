set script_path [ file dirname [ file normalize [ info script ] ] ]
source [file join $script_path architecture.tcl]

set blockname [file rootname [file tail [info script] ]]
set full_blockname "VectorUnit<$OUTPUT_DATATYPE, $DIMENSION, $DIMENSION>"
set full_blockname_stripped [string map {" " ""} $full_blockname]

source [file join $script_path common.tcl]

solution library add {[Block] FetchUnit.v1}
solution library add {[Block] ScaleUnit.v1}
solution library add {[Block] LayerNormUnit.v1}
solution library add {[Block] OutputAddressGenerator.v1}
solution library add {[Block] SoftmaxUnit.v1}

go libraries

directive set -CLOCKS $clocks

directive set /$full_blockname_stripped/FetchUnit<$DIMENSION> -MAP_TO_MODULE {[Block] FetchUnit.v1}
directive set /$full_blockname_stripped/ScaleUnit<$OUTPUT_DATATYPE,$DIMENSION,$DIMENSION> -MAP_TO_MODULE {[Block] ScaleUnit.v1}
directive set /$full_blockname_stripped/LayerNormUnit<$OUTPUT_DATATYPE,$DIMENSION> -MAP_TO_MODULE {[Block] LayerNormUnit.v1}
directive set /$full_blockname_stripped/OutputAddressGenerator<$OUTPUT_DATATYPE,$DIMENSION,$DIMENSION> -MAP_TO_MODULE {[Block] OutputAddressGenerator.v1}
directive set /$full_blockname_stripped/SoftmaxUnit<$OUTPUT_DATATYPE,$DIMENSION> -MAP_TO_MODULE {[Block] SoftmaxUnit.v1}

go assembly

go extract
