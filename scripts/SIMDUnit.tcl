set blockname [file rootname [file tail [info script] ]]

source scripts/common.tcl

directive set -DESIGN_HIERARCHY { 
    {SIMDUnit<16, 16>} 
}

go compile

source scripts/set_libraries.tcl

go libraries

directive set -CLOCKS $clocks

go assembly
directive set /SIMDUnit<16,16>/run/main -PIPELINE_STALL_MODE flush                                                    
directive set /SIMDUnit<16,16>/run/maxpool_comparator.value -WORD_WIDTH 128

go architect
 
go allocate

go extract
