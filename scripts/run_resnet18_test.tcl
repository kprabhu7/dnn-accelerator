# Check if test already exists
if {[file isdirectory resnet18_test]} { 
    project load resnet18_test
} else {
    project new -name resnet18_test
    project save
}

options set Input/TargetPlatform x86_64
options set /Input/CppStandard c++11
solution options set /Input/CppStandard c++11
options set Input/SearchPath ./src
options set Output/OutputVHDL false
options set Architectural/DefaultRegisterThreshold 4096

flow package require /SCVerify
flow package option set /SCVerify/USE_CCS_BLOCK true

flow package require /OSCI
flow package option set /OSCI/COMP_FLAGS {-Wall -Wno-unknown-pragmas -Wno-unused-label -O3}

set clk_period 5.0
set clocks "clk \"-CLOCK_PERIOD $clk_period -CLOCK_EDGE rising -CLOCK_HIGH_TIME [expr $clk_period/2] -CLOCK_OFFSET 0.000000 -CLOCK_UNCERTAINTY 0.0 -RESET_KIND async -RESET_SYNC_NAME rst -RESET_SYNC_ACTIVE high -RESET_ASYNC_NAME arst_n -RESET_ASYNC_ACTIVE low -ENABLE_NAME {} -ENABLE_ACTIVE high\" "

go new
solution file add ./src/conv_top.cpp -type CHEADER
solution file add ./src/tb_resnet18_val.cpp -type C++ -exclude true

go analyze

directive set -DESIGN_HIERARCHY { 
    {conv}
}

go compile
