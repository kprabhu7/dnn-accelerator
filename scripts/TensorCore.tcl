set script_path [ file dirname [ file normalize [ info script ] ] ]
source [file join $script_path architecture.tcl]

set blockname [file rootname [file tail [info script] ]]
set full_blockname "TensorCore<$INPUT_DATATYPE, $WEIGHT_DATATYPE, $OUTPUT_DATATYPE, $DIMENSION, $DIMENSION, $INPUT_BUFFER_SIZE, $WEIGHT_BUFFER_SIZE, $ACCUM_BUFFER_SIZE>"
set full_blockname_stripped [string map {" " ""} $full_blockname]

source [file join $script_path common.tcl]

solution library add {[Block] InputController.v1}
solution library add {[Block] WeightController.v1}
solution library add {[Block] MatrixProcessor.v1}
solution library add {[Block] VectorUnit.v1}

go libraries
directive set -CLOCKS $clocks

directive set /$full_blockname_stripped/InputController<$INPUT_DATATYPE,$DIMENSION> -MAP_TO_MODULE {[Block] InputController.v1}
directive set /$full_blockname_stripped/WeightController<$INPUT_DATATYPE,$DIMENSION,$DIMENSION> -MAP_TO_MODULE {[Block] WeightController.v1}
directive set /$full_blockname_stripped/MatrixProcessor<$INPUT_DATATYPE,$WEIGHT_DATATYPE,$OUTPUT_DATATYPE,$DIMENSION,$DIMENSION,$ACCUM_BUFFER_SIZE> -MAP_TO_MODULE {[Block] MatrixProcessor.v1}
directive set /$full_blockname_stripped/VectorUnit<$OUTPUT_DATATYPE,$DIMENSION,$DIMENSION> -MAP_TO_MODULE {[Block] VectorUnit.v1}

go assembly

directive set /$full_blockname_stripped/DoubleBuffer<$INPUT_DATATYPE,$DIMENSION,$INPUT_BUFFER_SIZE>/DoubleBuffer<$INPUT_DATATYPE,$DIMENSION,$INPUT_BUFFER_SIZE>:mem0Run/mem0Run/mem0.value.d -WORD_WIDTH [expr $DIMENSION * 16]
directive set /$full_blockname_stripped/DoubleBuffer<$INPUT_DATATYPE,$DIMENSION,$WEIGHT_BUFFER_SIZE>/DoubleBuffer<$INPUT_DATATYPE,$DIMENSION,$WEIGHT_BUFFER_SIZE>:mem1Run/mem1Run/mem1.value.d -WORD_WIDTH [expr $DIMENSION * 16]

go architect

go extract
