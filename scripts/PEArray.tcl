set blockname [file rootname [file tail [info script] ]]

source scripts/common.tcl

directive set -DESIGN_HIERARCHY { 
    {PEArray<IDTYPE, ODTYPE, 9, 16, 16>}
}

go compile

source scripts/set_libraries.tcl

go libraries

directive set -CLOCKS $clocks

go assembly

directive set /PEArray<IDTYPE,ODTYPE,9,16,16>/run/psumIn.value:rsc -MAP_TO_MODULE {[Register]}
directive set /PEArray<IDTYPE,ODTYPE,9,16,16>/run/inputIn.value:rsc -MAP_TO_MODULE {[Register]}
directive set /PEArray<IDTYPE,ODTYPE,9,16,16>/run/weightIn:rsc -MAP_TO_MODULE {[Register]}

directive set /PEArray<IDTYPE,ODTYPE,9,16,16>/run/psumOut.value:rsc -MAP_TO_MODULE {[Register]}
directive set /PEArray<IDTYPE,ODTYPE,9,16,16>/run/inputOut.value:rsc -MAP_TO_MODULE {[Register]}
directive set /PEArray<IDTYPE,ODTYPE,9,16,16>/run/weightOut:rsc -MAP_TO_MODULE {[Register]}

go architect
go allocate
go extract