set script_path [ file dirname [ file normalize [ info script ] ] ]
source [file join $script_path architecture.tcl]

set blockname [file rootname [file tail [info script] ]]
set full_blockname "AcceleratorTop"
set full_blockname_stripped [string map {" " ""} $full_blockname]

source [file join $script_path common.tcl]

solution library add {[Block] TensorCore.v1}

go libraries
directive set -CLOCKS $clocks

directive set /$full_blockname_stripped/TensorCore<$INPUT_DATATYPE,$WEIGHT_DATATYPE,$OUTPUT_DATATYPE,$DIMENSION,$DIMENSION,$INPUT_BUFFER_SIZE,$WEIGHT_BUFFER_SIZE,$ACCUM_BUFFER_SIZE> -MAP_TO_MODULE {[Block] TensorCore.v1}

go extract
