set script_path [ file dirname [ file normalize [ info script ] ] ]
source [file join $script_path architecture.tcl]

set blockname [file rootname [file tail [info script] ]]
set full_blockname "MatrixProcessor<$INPUT_DATATYPE, $WEIGHT_DATATYPE, $OUTPUT_DATATYPE, $DIMENSION, $DIMENSION, $ACCUM_BUFFER_SIZE>"
set full_blockname_stripped [string map {" " ""} $full_blockname]

source [file join $script_path common.tcl]

go libraries
directive set -CLOCKS $clocks

go assembly

directive set /MatrixProcessor<$INPUT_DATATYPE,$WEIGHT_DATATYPE,$OUTPUT_DATATYPE,$DIMENSION,$DIMENSION,$ACCUM_BUFFER_SIZE>/MatrixProcessor<$INPUT_DATATYPE,$WEIGHT_DATATYPE,$OUTPUT_DATATYPE,$DIMENSION,$DIMENSION,$ACCUM_BUFFER_SIZE>:run/run/while:accumulation_buffer.value.d -WORD_WIDTH [expr $DIMENSION * 16]

go architect

ignore_memory_precedences -from WRITE_ACC_BUFFER* -to READ_ACC_BUFFER*

go extract
