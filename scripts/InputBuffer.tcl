set blockname [file rootname [file tail [info script] ]]

source scripts/common.tcl

directive set -DESIGN_HIERARCHY { 
    {InputBuffer<16>}
}

go compile

source scripts/set_libraries.tcl

go libraries
directive set -CLOCKS $clocks

go assembly

directive set /InputBuffer<16>/input_buffer_0:cns -MAP_TO_MODULE ccs_sample_mem.ccs_ram_sync_dualport
directive set /InputBuffer<16>/input_buffer_1:cns -MAP_TO_MODULE ccs_sample_mem.ccs_ram_sync_dualport
directive set /InputBuffer<16>/InputReader<16>/run/for:for:for:for:tmp_0.data.value:rsc -MAP_TO_MODULE ccs_sample_mem.ccs_ram_sync_dualport
directive set /InputBuffer<16>/InputReader<16>/run/for:for:for:for:tmp_1.data.value:rsc -MAP_TO_MODULE ccs_sample_mem.ccs_ram_sync_dualport
directive set /InputBuffer<16>/InputWriter<16>/run/for:for:for:for:tmp_0.data.value:rsc -MAP_TO_MODULE ccs_sample_mem.ccs_ram_sync_dualport
directive set /InputBuffer<16>/InputWriter<16>/run/for:for:for:for:tmp_1.data.value:rsc -MAP_TO_MODULE ccs_sample_mem.ccs_ram_sync_dualport

directive set /InputBuffer<16>/StrideRegister<16>/input_grouped:rsc -MAP_TO_MODULE ccs_ioport.ccs_out_pipe
directive set /InputBuffer<16>/StrideRegister<16>/input_grouped:rsc -PACKING_MODE sidebyside
directive set /InputBuffer<16>/StrideRegister<16>/input_grouped -WORD_WIDTH 1152


directive set /InputBuffer<16>/StrideRegister<16>/run/TOTAL_COUNT_LOOP:group.value:rsc -MAP_TO_MODULE {[Register]}
directive set /InputBuffer<16>/StrideRegister<16>/run/MULTI_FIFO:group_0.value:rsc -MAP_TO_MODULE {[Register]}
directive set /InputBuffer<16>/StrideRegister<16>/run/MULTI_FIFO:group_1.value:rsc -MAP_TO_MODULE {[Register]}
directive set /InputBuffer<16>/StrideRegister<16>/run/MULTI_FIFO:group_2.value:rsc -MAP_TO_MODULE {[Register]}

directive set /InputBuffer<16>/InputReader<16>/buffer_0 -WORD_WIDTH 128
directive set /InputBuffer<16>/InputReader<16>/buffer_1 -WORD_WIDTH 128

directive set /InputBuffer<16>/InputWriter<16>/input_buffer_0 -WORD_WIDTH 128
directive set /InputBuffer<16>/InputWriter<16>/input_buffer_1 -WORD_WIDTH 128

directive set /InputBuffer<16>/input_buffer_0:cns -STAGE_REPLICATION 2
directive set /InputBuffer<16>/input_buffer_1:cns -STAGE_REPLICATION 2


directive set /InputBuffer<16>/input_buffer_0 -WORD_WIDTH 128
directive set /InputBuffer<16>/input_buffer_1 -WORD_WIDTH 128


directive set /InputBuffer<16>/inputs_to_sys:rsc -MAP_TO_MODULE ccs_ioport.ccs_out_pipe
directive set /InputBuffer<16>/inputs_to_sys:rsc -PACKING_MODE sidebyside

directive set /InputBuffer<16>/inputs_to_sys -WORD_WIDTH 1152


directive set /InputBuffer<16>/InputWriter<16>/run/for:for:for:for:tmp_0.data.value -WORD_WIDTH 128
directive set /InputBuffer<16>/InputWriter<16>/run/for:for:for:for:tmp_1.data.value -WORD_WIDTH 128

directive set /InputBuffer<16>/InputReader<16>/run/for:for:for:for:tmp_0.data.value -WORD_WIDTH 128
directive set /InputBuffer<16>/InputReader<16>/run/for:for:for:for:tmp_1.data.value -WORD_WIDTH 128

go architect

go allocate

go extract
