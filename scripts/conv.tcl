set blockname [file rootname [file tail [info script] ]]

source scripts/common.tcl

directive set -DESIGN_HIERARCHY { 
    {conv}
}

go compile

source scripts/set_libraries.tcl

solution library add {[Block] InputBuffer<16>.v1}
solution library add {[Block] SystolicArray<IDTYPE,ODTYPE,9,16,16>.v1}
solution library add {[Block] WeightFetcher<16>.v1}
solution library add {[Block] WeightOrganizer<16>.v1}
solution library add {[Block] BiasCombiner<16>.v1}
solution library add {[Block] SIMDUnit<16,16>.v1}
solution library add {[Block] IdentityFetcher<16>.v1}

go libraries

directive set -CLOCKS $clocks

directive set /conv/SystolicArray<IDTYPE,ODTYPE,9,16,16> -MAP_TO_MODULE {[Block] SystolicArray<IDTYPE,ODTYPE,9,16,16>.v1}
directive set /conv/InputBuffer<16> -MAP_TO_MODULE {[Block] InputBuffer<16>.v1}
directive set /conv/WeightFetcher<16> -MAP_TO_MODULE {[Block] WeightFetcher<16>.v1}
directive set /conv/WeightOrganizer<16> -MAP_TO_MODULE {[Block] WeightOrganizer<16>.v1}
directive set /conv/BiasCombiner<16> -MAP_TO_MODULE {[Block] BiasCombiner<16>.v1}
directive set /conv/SIMDUnit<16,16> -MAP_TO_MODULE {[Block] SIMDUnit<16,16>.v1}
directive set /conv/IdentityFetcher<16> -MAP_TO_MODULE {[Block] IdentityFetcher<16>.v1}

go assembly

directive set /conv/weightFetcherParams:cns -FIFO_DEPTH 1
directive set /conv/inputs_to_sys:cns -FIFO_DEPTH 2
directive set /conv/weights_to_sys:cns -FIFO_DEPTH 2
directive set /conv/combined_bias:cns -FIFO_DEPTH 2
directive set /conv/systolicArrayParams:cns -FIFO_DEPTH 1
directive set /conv/inputBufferParams:cns -FIFO_DEPTH 1
directive set /conv/systolicArrayOutputChannel:cns -FIFO_DEPTH 1
directive set /conv/simdUnitParams:cns -FIFO_DEPTH 1
directive set /conv/identityFetcherParams:cns -FIFO_DEPTH 1
directive set /conv/weightOrganizerParams:cns -FIFO_DEPTH 1

go architect

go allocate

go extract
