set script_path [ file dirname [ file normalize [ info script ] ] ]
source [file join $script_path architecture.tcl]

set blockname [file rootname [file tail [info script] ]]
set full_blockname "ScaleUnit<$OUTPUT_DATATYPE, $DIMENSION, $DIMENSION>"
set full_blockname_stripped [string map {" " ""} $full_blockname]

source [file join $script_path common.tcl]

go libraries
directive set -CLOCKS $clocks

go extract
