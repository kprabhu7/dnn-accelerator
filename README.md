# Inference Accelerator for Transformers
This project generates a hardware accelerator for Transformers, given a set of
parameters. 

## Description
Set the parameters for the design in src/ArchitectureParams.h and
scripts/architecture.tcl

## Requirements
Tools:
- Boost C++ Library
- Catapult HLS Installation

Environment Variables:
- BOOST_HOME 
- CATAPULT_HOME


## High-Level Synthesis (Verilog Generation)
Note: This requires a Catapult HLS License

```
$ mkdir build
$ make -f scripts/Makefile rtl
```

The generated Verilog can be found in build/ and can be pushed to downstream
EDA tools.
